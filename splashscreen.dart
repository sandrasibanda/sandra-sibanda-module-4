import 'package:flutter/material.dart';
import 'package:animated_splash_screen/animated_splash_screen.dart';
import 'package:module4_proj/home.dart';

class Splash extends StatelessWidget {
  const Splash({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    return MaterialApp(
        home: AnimatedSplashScreen(
      splash: 'assets/splash.png',
      nextScreen: const HomePage(),
      splashTransition: SplashTransition.fadeTransition,
      duration: 5000,
      
    
    ));
  }
}
