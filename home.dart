import 'package:flutter/material.dart';
import 'package:module4_proj/home.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home : Scaffold(
        appBar: AppBar(
          title: const Text('HOME'),
        ),
        body: const Center(
         child: Text("Module 2 Assessment",style: TextStyle(fontSize: 50,color: Colors.amber)),
         ),
      ),
    );
  }
}